﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pizzaprogram
{
    class Program
    {
        public class Client
        {
            public string Name;
            public string Number;

            public Client (string name, string number)
            {
                Name = name;
                Number = number;
            }

        }

        public static List<Tuple<string, string>> PizzaOrder = new List<Tuple<string, string>>();


        public class Pizza
        {
            public static int small = 5;
            public static int regular = 10;
            public static int large = 15;

            public string Hawaiian;
            public string Paper;
            public string Vegetarian;
            public string Cannibal;
            
            public static int cost;

            public static string Size;
            public static string Type;

            public static List<Tuple<string, string>> PizzaOrder = new List<Tuple<string, string>>();

            public Pizza(string size, string type)
            {
                if (size == "s" || size == "S") 
                {
                    Size = "Small";
                    cost = small;
                }
                else if (size == "r" || size == "R")
                {
                    Size = "Regular";
                    cost = regular;
                }
                else if (size == "l" || size == "L")
                {
                    Size = "Large";
                    cost = large;
                }

                //type//

                if (type == "h" || type == "H")
                {
                    Type = "Hawaiian";
                }
                else if (type == "p" || type == "P")
                {
                    Type = "Paper";
                }
                else if (type == "v" || type == "V")
                {
                    Type = "Vegetarian";
                }
                else if (type == "c" || type == "C")
                {
                    Type = "Cannibal";
                }
            }

            public int getCost()
            {
                return cost;
            }
        } 

        public class Drink
        {
            public static int apple = 5;
            public static int orange = 5;
            public static int carrot = 6;
            public static int sulfacid = 2;

            public static int drinkcost;
            public static string drinktype;
            public static string Dtype;

            public static List<Tuple<string>> DrinkOrder = new List<Tuple<string>>();

            public Drink(string drinktype)
            {

                if (drinktype == "a" || drinktype == "A")
                {
                    Dtype = "Apple Juice";
                    drinkcost = apple;
                }
                else if (drinktype == "o" || drinktype == "O")
                {
                    Dtype = "Orange Juice";
                    drinkcost = orange;
                }
                else if (drinktype == "c" || drinktype == "C")
                {
                    Dtype = "Carrot Juice";
                    drinkcost = carrot;
                }
                else if (drinktype == "s" || drinktype == "S")
                {
                    Dtype = "Sulfuric Acid";
                    drinkcost = sulfacid;
                }

            }
            public int getdrinkcost()
            {
                return drinkcost;
            }

        }

        public class main
        {
            public static Client cinput;
            public static List<Pizza> pizzas = new List<Pizza>();
            public static int index = 0;
            public static List<Drink> drinklist = new List<Drink>();
            public static int drinkindex = 0;

            static void Main(string[] args)
            {
                orderdetails();
            }

            public static void orderdetails()
            {
                Console.Clear();
                Console.WriteLine("Welcome to the Pizza Identifying Service System");
                Console.WriteLine();

                Console.WriteLine("Please enter a name for your order");
                string name = Console.ReadLine();
                Console.WriteLine();

                Console.WriteLine("Please enter your phone number.");
                string number = Console.ReadLine();
                Console.WriteLine();

                cinput = new Client(name, number);

                Console.WriteLine($"So your name is {cinput.Name} and your phone number is {cinput.Number}?");
                Console.WriteLine();

                Console.WriteLine("Type m to begin ordering, or type b to enter your details again");
                var detailinput = Console.ReadLine();

                if (detailinput == "m")
                {
                    menu();
                }
                else if (detailinput == "b")
                {
                    Console.Clear();
                    orderdetails();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("The input you have given is invalid. Press ENTER to try again");
                    Console.ReadLine();
                    orderdetails();
                }
            }

            //mainmenu//

            public static void menu()
            {
                Console.Clear();
                Console.WriteLine("**************************************************");
                Console.WriteLine("*                    Pizza Menu                  *");
                Console.WriteLine("*                                                *");
                Console.WriteLine("*           To order a pizza, enter o            *");
                Console.WriteLine("*                                                *");
                Console.WriteLine("*           To order a drink, enter d            *");
                Console.WriteLine("*                                                *");
                Console.WriteLine("*          To finish your order, enter f         *");
                Console.WriteLine("*                                                *");
                Console.WriteLine("**************************************************");

                var userinput = Console.ReadLine();

                if (userinput == "o")
                {
                    Console.Clear();
                    pizzamenu();
                }

                else if (userinput == "d")
                {
                    Console.Clear();
                    drinkmenu();
                }

                else if (userinput == "f")
                {
                    Console.Clear();
                    finish();
                }

                else
                {
                    Console.Clear();
                    Console.WriteLine("You have entered an invalid value. Please try again");
                    Console.WriteLine();
                    Console.WriteLine("Press enter to return to the main menu.");
                    Console.ReadLine();
                    menu();
                }

            }

            static void pizzamenu()
            {
                Console.WriteLine("****************************************");
                Console.WriteLine("*                                      *");
                Console.WriteLine("*   Enter the type of pizza you want   *");
                Console.WriteLine("*                                      *");
                Console.WriteLine("*             h = Hawaiian             *");
                Console.WriteLine("*             p = Paper                *");
                Console.WriteLine("*             v = Vegetarian           *");
                Console.WriteLine("*             c = Cannibal             *");
                Console.WriteLine("*                                      *");
                Console.WriteLine("****************************************");

                Pizza.Type = Console.ReadLine();

                if (Pizza.Type == "h" || Pizza.Type == "H" || Pizza.Type == "p" || Pizza.Type == "P" || Pizza.Type == "v" || Pizza.Type == "V" || Pizza.Type == "c" || Pizza.Type == "C")
                {
                    Console.Clear();

                    Console.WriteLine("****************************************");
                    Console.WriteLine("*                                      *");
                    Console.WriteLine("*   Enter the size of pizza you want   *");
                    Console.WriteLine("*                                      *");
                    Console.WriteLine("*             s = Small   | 5$         *");
                    Console.WriteLine("*             r = Regular | 10$        *");
                    Console.WriteLine("*             l = Large   | 15$        *");
                    Console.WriteLine("*                                      *");
                    Console.WriteLine("****************************************");

                    Pizza.Size = Console.ReadLine();

                    if (Pizza.Size == "s" || Pizza.Size == "S" || Pizza.Size == "r" || Pizza.Size == "R" || Pizza.Size == "l" || Pizza.Size == "L")
                    {
                        pizzas.Add(new Pizza(Pizza.Size, Pizza.Type));
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("You have selected a letter that does not correspond to a pizza size. Press ENTER to try again");
                        Console.ReadLine();
                        Console.Clear();
                        pizzamenu();
                    }
                }
                else if (Pizza.Type != "h" || Pizza.Type != "H" || Pizza.Type != "p" || Pizza.Type != "P" || Pizza.Type != "v" || Pizza.Type != "V" || Pizza.Type != "c" || Pizza.Type != "C")
                {
                    Console.Clear();
                    Console.WriteLine("You have selected a letter that does not correspond to a pizza type. Press ENTER to try again");
                    Console.ReadLine();
                    Console.Clear();
                    pizzamenu();
                }

                Console.WriteLine($"So you wanted a {Pizza.Size} {Pizza.Type}?");
                Console.WriteLine();
                Console.WriteLine("Type y to confirm, or type n to enter your order again");

                string pizzaorderconfirm = Console.ReadLine();

                Console.WriteLine();
                if (pizzaorderconfirm == "y" || pizzaorderconfirm == "Y")
                {
                    Pizza.PizzaOrder.Add(Tuple.Create(Pizza.Size, Pizza.Type));

                    index++;
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine($"Ok {cinput.Name}, you're doing great");
                    Console.WriteLine();
                    Console.WriteLine("Now we will go back to the menu and you can order a drink, or you can order more pizza if you want");
                    Console.WriteLine();
                    Console.WriteLine("Press ENTER to return to the menu");

                    Console.ReadLine();
                }
                else if (pizzaorderconfirm == "n" || pizzaorderconfirm == "N")
                {
                    pizzas.RemoveAt(pizzas.Count - 1);
                    Console.Clear();
                    pizzamenu();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("The character you have entered is invalid. Please try again");
                    Console.WriteLine();
                    Console.WriteLine($"Look {cinput.Name}, there are only 2 buttons to press. It shouldn't be that hard, even for you");
                    Console.WriteLine();
                    Console.WriteLine("Lets go back to the Pizza Menu and try again shall we? Press ENTER to proceed");
                    Console.WriteLine();
                    Console.ReadLine();
                }
                menu();
            }

            static void drinkmenu()
            {
                Console.WriteLine("****************************************");
                Console.WriteLine("*                                      *");
                Console.WriteLine("*   Enter the type of drink you want   *");
                Console.WriteLine("*                                      *");
                Console.WriteLine("*         a = Apple Juice   | $5       *");
                Console.WriteLine("*         o = Orange Juice  | $5       *");
                Console.WriteLine("*         c = Carrot Juice  | $6       *");
                Console.WriteLine("*         s = Sulfuric Acid | $2       *");
                Console.WriteLine("*                                      *");
                Console.WriteLine("****************************************");

                Drink.drinktype = Console.ReadLine();

                if (Drink.drinktype == "a" || Drink.drinktype == "A" || Drink.drinktype == "o" || Drink.drinktype == "O" || Drink.drinktype == "c" || Drink.drinktype == "C" || Drink.drinktype == "s" || Drink.drinktype == "S")
                {
                    drinklist.Add(new Drink(Drink.drinktype));
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("You have selected a letter that does not correspond to a drink type. Press ENTER to try again");
                    Console.ReadLine();
                    Console.Clear();
                    drinkmenu();
                }

                Console.WriteLine($"So you wanted a cup of {Drink.Dtype}?");
                Console.WriteLine();
                Console.WriteLine("Type y to confirm, or type n to enter your order again");

                string drinkorderconfirm = Console.ReadLine();

                Console.WriteLine();
                if (drinkorderconfirm == "y" || drinkorderconfirm == "Y")
                {
                    Drink.DrinkOrder.Add(Tuple.Create(Drink.Dtype));

                    drinkindex++;
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine($"Ok {cinput.Name}, you're doing great");
                    Console.WriteLine();
                    Console.WriteLine("Now we will go back to the menu and you can order pizza, or you can order more drinks if you want");
                    Console.WriteLine();
                    Console.WriteLine("Press ENTER to return to the menu");

                    Console.ReadLine();
                }

                else if (drinkorderconfirm != "y" || drinkorderconfirm != "Y" || drinkorderconfirm != "n" || drinkorderconfirm != "N")
                {
                    Console.Clear();
                    Console.WriteLine("The character you have entered is invalid. Please try again");
                    Console.WriteLine();
                    Console.WriteLine($"Ok {cinput.Name}, at this point I'm 90% sure that you are a plant");
                    Console.WriteLine();
                    Console.WriteLine("Lets go back to the Drinks Menu and try again shall we? Press ENTER to proceed");
                    Console.WriteLine();
                    Console.ReadLine();
                    Console.Clear();
                }

                else
                {
                    drinklist.RemoveAt(drinklist.Count - 1);
                    Console.Clear();
                    drinkmenu();
                }

                menu();

            }

            static void  finish()
            {
                Console.WriteLine($"We're almost done {cinput.Name}, we just need to confirm your details");
                Console.WriteLine();
                Console.WriteLine($"Is your number still {cinput.Number}? If it is, press y.");
                Console.WriteLine();
                Console.WriteLine("If it is not, press m to return to the menu and you can place your order again");

                string finconfirm = Console.ReadLine();

                if (finconfirm == "y" || finconfirm == "Y")
                {
                    Console.Clear();
                    Console.WriteLine($"Great, we're doing well! Now we just need to check your order");
                    Console.WriteLine();
                    Console.WriteLine($"So, you ordered {Pizza.PizzaOrder.Count} pizza(s) and {drinklist.Count} drink(s)");
                    Console.WriteLine($"You have ordered:");
                    Console.WriteLine();

                    for(var i = 0; i < Pizza.PizzaOrder.Count; i++)
                    {
                        var a = Pizza.Size = Pizza.PizzaOrder[i].Item1;
                        var b = Pizza.Type = Pizza.PizzaOrder[i].Item2;

                        Console.WriteLine($"{a} {b}");
                    }

                    Console.WriteLine();

                    for (var x = 0; x < drinklist.Count; x++)
                    {
                        var c = Drink.drinktype = Drink.DrinkOrder[x].Item1;

                        Console.WriteLine($"{c}");
                    }

                    Console.WriteLine();
                    Console.WriteLine("Is this order correct?");
                    Console.WriteLine();
                    Console.WriteLine("If this order is correct, type y. If it is incorrect, type m to return to the menu and try again");

                    string userconf = Console.ReadLine();

                    if (userconf == "y" || userconf == "Y")
                    {
                        payment();
                    }
                    else if (userconf == "m" || userconf == "M")
                    {
                        Console.Clear();
                        menu();
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("You make me want to crash myself. Press ENTER to agree to stop entering invalid values and to confirm your details again");
                        Console.ReadLine();
                        Console.Clear();
                        finish();
                        Console.ReadLine();
                    }

                    Console.ReadLine();
                }
                else
                {
                    Console.Clear();
                    menu();
                }
            }

            static void payment()
            {
                double pizzatotal = 0;
                double drinktotal = 0;

                for (int i = 0; i < pizzas.Count; i++)
                {
                    pizzatotal = pizzatotal + pizzas[i].getCost();
                }

                for (int x = 0; x < drinklist.Count; x++)
                {
                    drinktotal = drinktotal + drinklist[x].getdrinkcost();
                }

                double total = pizzatotal + drinktotal;

                Console.Clear();
                Console.WriteLine($"Your pizza(s) come to ${pizzatotal} and your drink(s) come to ${drinktotal}");
                Console.WriteLine();
                Console.WriteLine($"Your total cost is ${total}");
                Console.WriteLine();
                Console.WriteLine($"Please enter the amount of money you will be paying and I will calculate your change");

                var userpay = Console.ReadLine();
                int parsedpay;
                
                bool parsed = Int32.TryParse(userpay, out parsedpay);

                if (parsedpay < total)
                {
                    Console.Clear();
                    Console.WriteLine($"Nice try {cinput.Name}. You need to pay me full price for your pizza, or I will send your number: {cinput.Number} to telemarketers");
                    Console.WriteLine("Press ENTER to try again and please stop being difficult");
                    Console.ReadLine();
                    payment();
                }
                else if (!parsed)
                {
                    Console.WriteLine($"You have entered an invalid value.");
                    Console.WriteLine("Do not trifle with your Pizza Overlords");
                    Console.WriteLine("Please press ENTER to try again");
                    Console.ReadLine();
                    Console.Clear();
                    payment();
                }
                else if (parsedpay > total || parsedpay == total)
                {
                    double change = parsedpay - total;

                    Console.WriteLine();
                    Console.WriteLine($"Your order came to ${total} and you paid ${parsedpay}");
                    Console.WriteLine();

                    Console.WriteLine($"Your change is ${change}");
                    Console.WriteLine("Press ENTER to end the program");
                    Console.ReadLine();
                    ascpizz();
                }
            }

            static void ascpizz()
            {
                Console.Clear();
                Console.WriteLine($"Thank you {cinput.Name} for using the Pizza Identifying Service System!");
                Console.WriteLine();
                Console.WriteLine("           _....._");
                Console.WriteLine("       _.:`.--|--.`:._");
                Console.WriteLine("     .: .'.o  | o /'. '.");
                Console.WriteLine("    // '.  o o|  /  o ',|");
                Console.WriteLine("   //'._o'. . |o/ o_.-'o\\");
                Console.WriteLine("   || o '-.'./|/.-' o   ||");
                Console.WriteLine("   ||--o--o-->|<o-----o-||");
                Console.WriteLine("   \\  o _.- '/|\'-._o  //");
                Console.WriteLine("    \\.-'  o/ |o| o '-.//");
                Console.WriteLine("     '.'.o / o|  | o.'.'");
                Console.WriteLine("       `-:/.__|__o|:-'");

                Console.ReadLine();

                Environment.Exit(0);



            }

        }
    }
}
